<?php /* $Id$ */
/**
 * status codes
 */
function _guestpass_status($key=0) {
  $status = array(
    1 => t('invited'),
    2 => t('accepted'),
    3 => t('upgraded'),
    4 => t('expired'),
    5 => t('limit exceeded'),
  );
  return $key ? $status[$key] : $status;
}

/** 
 * Settings
 */
function _guestpass_settings() {
  $form = array();
  $roles = user_roles();
  $rid  = variable_get('guestpass_role',2);
  unset($roles[1]);              // anonymous user isn't a useful role
  $form['guestpass_role'] = array(
  	  '#type'          => 'radios',
    '#title'         => t('Guest Role'),
    '#default_value' => $rid,
    '#options'	      => $roles,
    '#description'   => t('Guest will temporarily be granted all rights for this role'),
  );
  
  // member/upgraded role(s)
  unset($roles[$rid]);            // looking to change roles
  if (count($roles)) {
    $form['guestpass_upgrade_role'] = array(
  	    '#type'          => 'select',
  	    '#multiple'      => 'select',
      '#title'         => t('Member Role'),
      '#default_value' => variable_get('guestpass_upgrade_role',0),
      '#options'	      => $roles,
      '#description'   => t("If a guest's role is changed to one of these, it will be noted as an escalation"),
    );
  }
  // duration of guestpass length
  
  // limit on guest passes
  // limit_count, limit_range
  // limit exceeded message + subject
  
  	// welcome message + subject
  // expiry message + subject
  return $form;
}

/**
 * Generate a guestpass
 * @return $guestpass - an encoded guestpass identifier
 */
function _guestpass_send_invite($sender, $recipient, $values=array()) {
  static $invite = array();
  $mail = $recipient['mail'];
  if (!isset($invite[$sid.$mail])) {
    $gid = db_next_id('{guestpass}_gid');
  
  		// find existing user if possible
    $res = db_query("SELECT uid FROM {users} WHERE mail='%s'", $mail);
    $uid = $res ? db_result($res) : 0;
  
    if (!$values['duration']) {
      $duration = variable_get('guestpass_duration','3 D');
    }
    db_query("INSERT INTO {guestpass} 
    		( gid, uid, sid, mail, timestamp, duration, status ) VALUES 
    		( %d, %d, %d, '%s', %d, '%s', 1 )", $gid, $uid, $sid, $mail, time(), $duration);
  
    $invite[$sid.$mail] = md5($mail.$sid.$gid._guestpass_secret()).$gid;
  }
  return $invite[$sid.$mail];
}

/**
 * Process an incoming guestpass identifier
 */
function _guestpass_accept($guestpass, $user=null) {
  $hash = substr($guestpass,0,32);
  $gid  = substr($guestpass,32);
  
  $row = db_fetch_object(db_query('SELECT * FROM {guestpass} WHERE gid=%d', $gid));
  
  if (!$row) {                   // nonexistant
    return false;
  }
  if (md5($row->mail.$row->sid.$gid._guestpass_secret()) != $hash) {
    return false;                // does not validate
  }
  if ($row->status == 2) {       // already activated
    return $row->uid;
  }
  if ($row->status != 1) {       // not "invited" status
    return false;
  }
  if ($user->uid) {
    $uid = $user->uid;
  }
  elseif ($row->uid == 0) {          // no uid yet - let's find/create one
	  if ($res = db_query("SELECT uid FROM {users} WHERE mail='%s'",$mail)) {
      $uid = db_result($row);
    }
    else {
      $user = user_save(null, $user = array(
				'name' => $row->mail,
				'mail' => $row->mail,
				'pass' => user_pass(),
				'roles' => array(_user_authenticated_id(), variable_get('guestpass_role',0)),
				'status' => 1,
       ));
       $uid = $user->uid;
       // send welcome email (account information)
    }
  }
  else {
    $uid = $row->uid;
  }
  
  $initialized = date('Y-m-d H:i', time());
  $expires     = _guestpass_expiry($initialized, $row->duration);
  $status      = _guestpass_user_count($uid) > variable_get('guestpass_limit',3) ? 5 : 2;
  
  db_query("UPDATE {guestpass} 
    SET status = %d, initialized = '%s', expires = '%s', uid = %d
  	  WHERE gid=%d", $status, $initialized, $expires, $uid, $gid);
      
  // update node_access table with new grants for all nodes in the message
  foreach(send_message_nodes($row->sid) as $nid) {
    db_query("DELETE FROM {node_access} WHERE gid=%d AND realm='guestpass'", $nid);
    db_query("INSERT INTO {node_access} (nid, gid, realm, grant_view)
      VALUES (%d, %d, 'guestpass', 1)", $nid, $nid); 
  }
  
  // send welcome email
  
  return $uid;
}

/**
 * User is upgraded to a valid account
 */
function _guestpass_upgrade($user) {
  db_query('UPDATE {guestpass} 
    SET status = 3, expires = NOW() WHERE uid=%d, status=2', $user->uid);
  _guestpass_set_role($user);
}

/**
 * Expire the guestpass
 */
function _guestpass_expire($gid, $notify=true) {
  db_query('UPDATE {guestpass} SET status = 4 WHERE gid=%d', $gid);
  
  // send expired email
  if ($notify && $body = variable_get('guestpass_expired_msg','')) {
    $row = db_fetch_object(db_query('SELECT * FROM {guestpass} WHERE gid=%d', $gid));
    
    $user    = user_load(array('uid' => $row));
    $subject = variable_get('guestpass_expired_sub', t('Guest Pass Expired'));
    $body    = $body;
    mimemail(null, $user, $subject, $body);
  }
}

/**
 * determine a users's guest status
 */
function _guestpass_user_status(&$user) {
  if (!$user->uid) return false;
  if (!$rid = variable_get('guestpass_role',0)) return false;
  if (!isset($user->roles[$rid])) return false;
  
  if($row = db_fetch_object(db_query('SELECT 1 FROM {guestpass} WHERE uid=%d AND status = 2', $uid))) {
    return true;
  }
  return false;
}

/**
 * set role based on guest status
 */
function _guestpass_set_role(&$user) {
  
  if (!$rid = variable_get('guestpass_role',0)) return false;
  
  $roles = $user->roles;
  if (_guestpass_user_status($user)) { // set role
    if (!isset($roles[$rid])) {
      $roles[$rid] = $rid;
      $user = user_save($user, array('roles' => $roles));
    }
  }
  else {                               // unset role
    if (isset($roles[$rid])) {
      unset($roles[$rid]);
      $user = user_save($user, array('roles' => $roles));
    }
  }
  return true;
}
function _guestpass_user_count($uid) {
  if ($uid == 0) return 0;
  
  // todo based on limit timeframe
  $start = date('Y-m-d H:i' );
      
  if ($row = db_query("SELECT COUNT(1) AS count FROM {guestpass} 
		WHERE uid=%d AND initialized >= '%s'", $uid, $start)) {
    
    return $row->count;
  }
  return 0;
}

function _guestpass_secret() {
  if (!$secret = variable_get('guestpass_secret',0)) {
    srand( ((int)((double)microtime()*1000003)) );
    $secret = md5(rand());
    variable_set('guestpass_secret', $secret);
  }
  return $secret;
}

function _guestpass_expiry($initialized, $duration) {
  list($c, $u) = explode(' ', $duration);
  $s = strtotime($initialized);
  
  return date('Y-m-d H:i', mktime(date('H',$s)+($u=='H')*$c, date('i',$s), 0, 
    date('m',$s)+($u=='M')*$c, date('d',$s)+($u=='D')*$c+($u=='W')*$c*7, date('y',$s)+($u=='Y')*$c));
}
